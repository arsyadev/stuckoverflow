<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Question extends Model
{
    //
    protected $fillable = ['title', 'body'];

    public function user(){
        return $this->belongTo(User::class);
    }
}
